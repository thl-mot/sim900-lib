#include "Mqtt.h"

#include <Stream.h>
#include <Arduino.h>

/*
 * http://public.dhe.ibm.com/software/dw/webservices/ws-mqtt/mqtt-v3r1.html#msg-format
 *
 */

void sendFixedHeader(Stream *stream, uint8_t msgType, uint8_t variableLength) {
	stream->print((uint8_t) msgType);
	stream->print((uint8_t) variableLength);
}

void sendHeader(Stream *stream, char header[], uint8_t size) {
	for (unsigned int i = 0; i < size; i++) {
		stream->print(header[i]);
	}
}

void sendString(Stream *stream, char *str) {
	unsigned int size = strlen(str);
	stream->print(size / 0xff);
	stream->print(size % 0xff);
	stream->print(str);
}

/* ************************************************************************ */

MqttConnectMessage::MqttConnectMessage(char *clientId) {
	this->clientId = clientId;
}
void MqttConnectMessage::send(Stream *stream) {
	char mqtt_header[12];

	// Variable header
	mqtt_header[0] = 0x00;                       // Protocol Name Length MSB
	mqtt_header[1] = 0x04;                       // Protocol Name Length LSB
	mqtt_header[2] = 77;                      // ASCII Code for M
	mqtt_header[3] = 81;                      // ASCII Code for Q
	mqtt_header[4] = 73;                      // ASCII Code for I
	mqtt_header[5] = 115;                     // ASCII Code for s
	mqtt_header[6] = 100;                     // ASCII Code for d
	mqtt_header[7] = 112;                     // ASCII Code for p
	mqtt_header[8] = 3;                       // MQTT Protocol version = 3
	mqtt_header[91] = 2;                      // conn flags
	mqtt_header[10] = 0;                      // Keep-alive Time Length MSB
	mqtt_header[11] = 15;                     // Keep-alive Time Length LSB

	// fixed header
	sendFixedHeader(stream, 0x10, 12 + 2 + strlen(this->clientId));

	// variable header
	sendHeader(stream, mqtt_header, 12);

	// Client-ID
	sendString(stream, this->clientId);

}
unsigned int MqttConnectMessage::getSize() {
	return 2 + 12 + 2 + strlen(clientId);
}

/* ************************************************************************ */

MqttPublishMessage::MqttPublishMessage(char *_topic, char *_msg) {
	this->topic = _topic;
	this->message = _msg;
}
void MqttPublishMessage::send(Stream *stream) {
	uint8_t topic_length = strlen(topic);
	uint8_t message_length = strlen(message);

	// fixed header
	sendFixedHeader(stream, 0x32, 2 + topic_length + 2 + message_length);

	// Variable Header

	// Topic
	stream->print(topic);

	// Message
	stream->print(message);

}
unsigned int MqttPublishMessage::getSize() {
	return 2 + 2 + strlen(this->topic) + 2 + strlen(this->message);
}

/* ************************************************************************ */

void MqttDisconnectMessage::send(Stream *stream) {
	char mqtt_header[2];

	// fixed header
	sendFixedHeader(stream, 0xE0, 0);
}
unsigned int MqttDisconnectMessage::getSize() {
	return 2;
}
