/*
 * Sim900.h
 *
 *  Created on: 11.02.2018
 *      Author: thomas
 */

#ifndef SIM900_H_
#define SIM900_H_

#include "MessageProducer.h"

#define STATE_IDLE 			    (0)
#define STATE_WAIT_OK 		    (1)
#define STATE_WAIT_PROMPT 	    (2)
#define STATE_WAIT_SMS_CONTENT	(3)
#define STATE_WAIT_HTTP_CONTENT (4)
#define STATE_WAIT_SHUT_OK	    (5)
#define STATE_WAIT_IP			(6)
#define STATE_WAIT_CONNECT_OK   (7)
#define STATE_WAIT_CLOSE_OK   	(8)
#define STATE_WAIT_SEND_OK		(9)
#define STATE_WAIT_TCP			(10)

#define RESULT_ERROR			(-1)
#define RESULT_IDLE				(0)
#define RESULT_OK				(1)
#define RESULT_PROMPT			(2)
#define RESULT_HTTP_AVAILABLE	(3)
#define RESULT_HTTP_RETRIEVING	(4)
#define RESULT_HTTP_DONE		(5)

#define HTTP_IDLE				(0)
#define HTTP_WAITRESPONSE		(1)
#define HTTP_RESPONSEREADY		(2)
#define HTTP_WAITCONTENT		(3)
#define HTTP_CONTENT		    (4)

#define CONNECT_DISCONNECT_TIMEOUT (10000)


class Sim900 {
private:
	unsigned long time;

	int  receiverState;

	char asyncBuffer[200];
	unsigned int  asyncPos;

	int httpState;
	unsigned int httpDataCount;

	int availableNetworks;

	bool sendCommand(const char at[], int state= STATE_WAIT_OK, unsigned long timeoutValue=500);

	bool wait( int state= STATE_WAIT_OK, unsigned long timeoutValue=500);
	bool setEchoOff();

	void resetTimeout();
	bool isTimeout( unsigned long timeoutValue=500);

public:
	Sim900();
	virtual ~Sim900();

	bool begin();

	bool cmdAT();
	bool signalQuality();
	bool checkNetworkRegistration();
	bool checkAvailableNetworks();

	bool initGPRS();

	bool tcpOpen( const char server[], unsigned int port);
	bool tcpSend( const char server[], unsigned int size);
	bool tcpSendMessage( MessageProducer *msgProducer);
	bool tcpClose();

	bool sendSMS( const char tel[], const char txt[]);
	bool listSMS();
	bool deleteSMS(unsigned int idx);

	bool http( const char url[]);

	int poll(int forcedState=STATE_IDLE);

};

#endif /* SIM900_H_ */
