# README #

This library is still under development. It's is not meant for use in production.

### What is this library for? ###

With this Library I'd like to support the SIM900 GSM-chip for Arduino-Projects. 

My goals for this library are

* keep it simple and small to save flash for the main purpose of your sketch.
* no dependencies to other libraries (except Arduino core libraries).
* free, "as is" usage in any of your projects


### Current Functionality ###

* Send SMS
* Make a basic http-Request

### Support ###

You are wellcome to support this library.

### License ###

free, "as is" usage, with no warrenty