/*
 * Sim900.cpp

 *
 *  Created on: 11.02.2018
 *      Author: Thomas Lauerbach
 */

#include "Sim900.h"
#include <SoftwareSerial.h>
#include <Arduino.h>

SoftwareSerial debug(16, 17);

Sim900::Sim900() {
	asyncPos = 0;
	receiverState = STATE_IDLE;
	httpState = HTTP_IDLE;
	time = 0;
	httpDataCount = 0;
	availableNetworks = -1;
}

Sim900::~Sim900() {
}

int getInt(char str[], unsigned int first, unsigned int last) {
	unsigned int result = 0;
	for (unsigned int i = first; i < strlen(str) && i < last; i++) {
		if (str[i] == 0 || (result>0 && str[i] == ' ') ) {
			break;
		}
		if (str[i] != ' ') {
			result = result * 10 + (str[i] - '0');
		}
	}
	return result;
}

bool Sim900::begin() {
	debug.begin(9600);
	debug.println("Sim900::init()");

	Serial.begin(19200);

	while (Serial.available()) {
		Serial.read();
	}

	while (!cmdAT()) {
		debug.print(".");
		delay(1000);
	}

	setEchoOff();

	asyncPos = 0;

	receiverState = 0;

	debug.println("Waiting for Networks");
	unsigned long t = millis();
	while ((millis() - t) < 30000) {
		if (!checkAvailableNetworks()) {
			return false;
		}
		if (availableNetworks > 0) {
			break;
		}
		delay(500);
	}
	debug.println("Networks available");

	sendCommand("AT+CGATT?\r");

	sendCommand("AT+CIPSHUT\r", STATE_WAIT_SHUT_OK, CONNECT_DISCONNECT_TIMEOUT);

	sendCommand("AT+CIPMUX=0\r");

	sendCommand("AT+CSTT=\"internet.telekom\"\r");
	sendCommand("AT+CIICR\r", STATE_WAIT_OK, CONNECT_DISCONNECT_TIMEOUT);
	sendCommand("AT+CIFSR\r", STATE_WAIT_IP);

	return true;

}

bool Sim900::sendCommand(const char at[], int state,
		unsigned long timeoutValue) {
	debug.print("DEBUG >Sim900:");
	debug.println(at);

	Serial.print(at);

	return wait(state, timeoutValue);
}

void Sim900::resetTimeout() {
	time = millis();
}
bool Sim900::isTimeout(unsigned long timeout) {
	bool result = ((millis() - time) > timeout);
	if (result) {
		debug.print("\r\nDEBUG TIMEOUT ");
		debug.println(timeout);
	}
	return result;
}

bool Sim900::wait(int state, unsigned long timeoutValue) {
	resetTimeout();
	while (!isTimeout(timeoutValue)) {
		if (poll(state) == RESULT_OK) {
			receiverState = STATE_IDLE;
			return true;
		}
	}
	return false;
}

int Sim900::poll(int state) {
	if (state == 0) {
		state = receiverState;
	}

	int result = RESULT_IDLE;

	if (Serial.available()) {
		resetTimeout();
		if (asyncPos >= sizeof(asyncBuffer) - 2) {
			debug.println("DEBUG buffer overflow");
			asyncPos = 0;
			return RESULT_ERROR;
		}
		char c = Serial.read();

//		debug.print( "State:");
//		debug.print( state);
//		debug.print( " Input:");
		debug.print( String(c,HEX)); debug.print(" ");
//		debug.print( " Buffer: '");
//		debug.print( asyncBuffer);
//		debug.print( "' ");
//		debug.println();

		if (state == STATE_WAIT_TCP) {
			debug.print(c);
		} else if (state == STATE_WAIT_HTTP_CONTENT) {
			if (httpDataCount == 0) {
				debug.println("DEBUG HTTP-Data received");
				receiverState = STATE_IDLE;
				httpState = HTTP_IDLE;
				result = RESULT_HTTP_DONE;
			}
			httpDataCount--;

		} else if (c == 13) {
			if (state == STATE_WAIT_OK && strcmp("OK", asyncBuffer) == 0) {
				debug.println("DEBUG OK");
				result = RESULT_OK;
				receiverState = STATE_IDLE;
			} else if (state == STATE_WAIT_SHUT_OK
					&& strcmp("SHUT OK", asyncBuffer) == 0) {
				debug.println("DEBUG SHUT OK");
				result = RESULT_OK;
				receiverState = STATE_IDLE;
			} else if (state == STATE_WAIT_CONNECT_OK
					&& strcmp("CONNECT OK", asyncBuffer) == 0) {
				debug.println("DEBUG CONNECT OK");
				result = RESULT_OK;
				receiverState = STATE_WAIT_TCP;
			} else if (state == STATE_WAIT_CLOSE_OK
					&& strcmp("CLOSE OK", asyncBuffer) == 0) {
				debug.println("DEBUG CLOSE OK");
				result = RESULT_OK;
				receiverState = STATE_IDLE;
			} else if (state == STATE_WAIT_SEND_OK
					&& strcmp("SEND OK", asyncBuffer) == 0) {
				debug.println("DEBUG SEND OK");
				result = RESULT_OK;
				receiverState = STATE_IDLE;
			} else if (strcmp("ERROR", asyncBuffer) == 0) {
				if (state == STATE_WAIT_OK) {
					debug.println("DEBUG ERROR");
					result = RESULT_ERROR;
					receiverState = STATE_IDLE;
				}
			} else if (strncmp("+CMT", asyncBuffer, 4) == 0) {
				debug.print("DEBUG incoming SMS='");
				debug.print(asyncBuffer);
				debug.println("'");
				receiverState = STATE_WAIT_SMS_CONTENT;
			} else if (strncmp("+CSQ", asyncBuffer, 4) == 0) {
				// Signal Quality
				debug.print("DEBUG CSQ='");
				debug.print(asyncBuffer);
				debug.println("'");
				receiverState = STATE_IDLE;
			} else if (strncmp("+CREG", asyncBuffer, 5) == 0) {
				// Check Network Registration
				debug.print("DEBUG CREG='");
				debug.print(asyncBuffer);
				debug.println("'");
				receiverState = STATE_IDLE;
			} else if (strncmp("+CMGS", asyncBuffer, 5) == 0) {
				// SMS sent
				debug.print("DEBUG SMS id'");
				debug.print( getInt(asyncBuffer, 6, 20));
				debug.println("'");
				receiverState = STATE_IDLE;
			} else if (strncmp("+CGATT", asyncBuffer, 6) == 0) {
				// available Networks
//				debug.print("DEBUG CGATT='");
//				debug.print(asyncBuffer);
//				debug.println("'");
				availableNetworks = asyncBuffer[8] - '0';
				receiverState = STATE_IDLE;
			} else if (strncmp("+HTTPACTION", asyncBuffer, 11) == 0) {
				// data received
				// +HTTPACTION:0,200,3
				debug.print("DEBUG HTTPACTION='");
				debug.print(asyncBuffer);
				debug.println("'");
				httpState = HTTP_RESPONSEREADY;
				receiverState = STATE_IDLE;
				result = RESULT_HTTP_AVAILABLE;
			} else if (strncmp("+HTTPREAD", asyncBuffer, 9) == 0) {
				httpDataCount = 0;
				for (unsigned int i = 10; i < strlen(asyncBuffer); i++) {
					httpDataCount = (httpDataCount * 10)
							+ (asyncBuffer[i] - '0');
				}

				debug.print("DEBUG HTTP-DATA-Size ");
				debug.print(httpDataCount);
				debug.println();

				receiverState = STATE_WAIT_HTTP_CONTENT;
				result = RESULT_HTTP_RETRIEVING;
			} else if (strncmp("+", asyncBuffer, 1) == 0) {
				debug.print("DEBUG unspecific event='");
				debug.print(asyncBuffer);
				debug.println("'");
				receiverState = STATE_IDLE;
			} else {
				// unspecific text Content
				if (strlen(asyncBuffer) > 0) {
					if (receiverState == STATE_WAIT_SMS_CONTENT) {
						debug.print("DEBUG SMS content='");
						debug.print(asyncBuffer);
						debug.println("'");
						receiverState = STATE_IDLE;
						result = RESULT_OK;
					} else if (state == STATE_WAIT_IP) {
						debug.print("DEBUG IP='");
						debug.print(asyncBuffer);
						debug.println("'");
						receiverState = STATE_IDLE;
						result = RESULT_OK;
					} else if (state == STATE_WAIT_CONNECT_OK
							&& strcmp("OK", asyncBuffer) == 0) {
						// ignore
					} else {
						// unexpected content
						debug.print("DEBUG unexpected content='");
						debug.print(asyncBuffer);
						debug.println("'");
					}
				} else {
					// empty line
					// do nothing
				}
			}
			asyncPos = 0;
			asyncBuffer[0] = 0;
		} else if (c != 10) {
			asyncBuffer[asyncPos++] = c;
			asyncBuffer[asyncPos] = 0;

			if (state == STATE_WAIT_PROMPT && asyncPos >= 2) {
				if ((asyncBuffer[asyncPos - 2] == '>')
						&& (asyncBuffer[asyncPos - 1] == ' ')) {
					asyncPos = 0;
					asyncBuffer[0] = 0;
					result = RESULT_OK;
				}
			}
		}
	} // serial available
	return result;
}

bool Sim900::setEchoOff() {
	return sendCommand("ATE0\r");
}

bool Sim900::cmdAT() {
	return sendCommand("AT\r");
}

bool Sim900::signalQuality() {
	return sendCommand("AT+CSQ\r");
}
bool Sim900::checkNetworkRegistration() {
	return sendCommand("AT+CREG?\r");
}

bool Sim900::checkAvailableNetworks() {
	return sendCommand("AT+CGATT?\r");
}

bool Sim900::tcpOpen(const char server[], unsigned int port) {

	sendCommand("AT+CIPQSEND=0\r");

	debug.println("DEBUG OPEN TCP");

	Serial.print("AT+CIPSTART=\"TCP\",\"");
	Serial.print(server);
	Serial.print("\",\"");
	Serial.print(port);
	Serial.print("\"\r");

	return wait( STATE_WAIT_CONNECT_OK, CONNECT_DISCONNECT_TIMEOUT);

}

bool Sim900::tcpClose() {
	return sendCommand("AT+CIPCLOSE\r", STATE_WAIT_CLOSE_OK);
}

bool Sim900::tcpSend(const char data[], unsigned int size) {
	debug.print("DEBUG send CIPSEND ");
	debug.println(size);

	Serial.print("AT+CIPSEND=");
	Serial.print(size);
	Serial.print("\r");
	if (!wait( STATE_WAIT_PROMPT, 1000)) {
		debug.print("DEBUG send CIPSEND canceled");
		return false;
	}

	debug.print("DEBUG sending data ");

	for (unsigned int i = 0; i < size; i++) {
		Serial.print(data[i]);
	}

	debug.println();

	return wait(STATE_WAIT_SEND_OK);
}

bool Sim900::tcpSendMessage(MessageProducer *prod) {
	unsigned int size= prod->getSize();

	debug.print("DEBUG tcpSendMessage ");
	debug.println( size);



	Serial.print("AT+CIPSEND=");
	Serial.print(size);
	Serial.print("\r");
	if (!wait( STATE_WAIT_PROMPT, 1000)) {
		debug.print("DEBUG send CIPSEND canceled");
		return false;
	}

	debug.print("DEBUG sending data ");

	prod->send( &Serial);

	debug.println();

	return wait(STATE_WAIT_SEND_OK);

}


bool Sim900::sendSMS(const char tel[], const char txt[]) {
	if (!sendCommand("AT+CMGF=1\r")) {
	}

	Serial.print("AT+CMGS=\"");
	Serial.print(tel);
	Serial.print("\"\r");

	if (!wait( STATE_WAIT_PROMPT)) {
		return false;
	}
	delay(1000);

	Serial.print(txt);
	Serial.println((char) 26);

	return wait();

}

bool Sim900::listSMS() {
	return sendCommand("AT+CMGL\r");
}

bool Sim900::deleteSMS(unsigned int idx) {
	Serial.print("AT+CMGD=");
	Serial.print(idx);
	Serial.print("\r");
	return wait();
}

/*
 * Not working at the moment
 */
bool Sim900::http(const char url[]) {
	// AT+HTTPINIT
	sendCommand("AT+HTTPINIT\r");

	// AT+HTTPPARA="URL","http://www.google.de"
	sendCommand(
			"AT+HTTPPARA=\"URL\",\"http://servix.lauerbach.com/test.json\"\r");

	// AT+HTTPACTION=0
	httpState = HTTP_WAITRESPONSE;
	unsigned long t = millis();
	sendCommand("AT+HTTPACTION=0\r");

	while (true) {
		if (poll() == RESULT_HTTP_AVAILABLE) {
			break;
		}
		if (millis() - t > 30000) {
			debug.println("DEBUG http-timeout");
			return false;
		}
	}

	// AT+HTTPREAD
	httpState = HTTP_WAITCONTENT;
	sendCommand("AT+HTTPREAD\r");

	// AT+HTTPTERM
	// sendCommand("AT+HTTPTERM\r");

}

