/*
  Blink.
  Turns on an LED on for one second, then off for one second, repeatedly.

  Most Arduinos have an on-board LED you can control. On the Uno and
  Leonardo, it is attached to digital pin 13. If you're unsure what
  pin the on-board LED is connected to on your Arduino model, check
  the documentation at http://www.arduino.cc

  This example code is in the public domain.

  modified 8 May 2014
  by Scott Fitzgerald
 */

#include <Arduino.h>
#include <SoftwareSerial.h>
#include "Sim900.h"
#include "Mqtt.h"

Sim900 sim900;
// TextMessage halloText("Hallo");

MqttConnectMessage connectMsg("sim900");
MqttPublishMessage publishMsg("test/test","inhalt");

void setup() {
	sim900.begin();

//	sim900.signalQuality();
//	sim900.checkNetworkRegistration();
//	sim900.checkAvailableNetworks();
//	sim900.attachGPRS();
//	sim900.setConnectionTypeToGPRS();
//	sim900.setAPN();
//	sim900.openGPRS();

	sim900.tcpOpen( "aws.lauerbach.de", 1883);

	// sim900.tcpSend( "Hallo", 5);
	// sim900.tcpSendMessage( &halloText);
	sim900.tcpSendMessage( &connectMsg);
	sim900.tcpSendMessage( &publishMsg);

	//sim900.tcpClose();

	// sim900.sendSMS("+491713745497", "Hallo dies ist ein Test");
	// sim900.http("");
	// sim900.listSMS();



}

void loop() {
	sim900.poll();
}
