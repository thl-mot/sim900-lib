/*
 * Mqtt.h
 *
 *  Created on: 18.02.2018
 *      Author: thomas
 */

#ifndef MQTT_H_
#define MQTT_H_

#include <string.h>
#include <stdint.h>
#include <Stream.h>
#include "MessageProducer.h"

class MqttConnectMessage : public MessageProducer {
	char *clientId;
public:
	MqttConnectMessage( char *clientId);
	virtual void send( Stream *stream);
	virtual unsigned int getSize();
};

class MqttPublishMessage : public MessageProducer {
	char *topic;
	char *message;
public:
	MqttPublishMessage( char *topic, char *msg);
	virtual void send( Stream *stream);
	virtual unsigned int getSize();
};

class MqttDisconnectMessage : public MessageProducer {
public:
	virtual void send( Stream *stream);
	virtual unsigned int getSize();
};

#endif /* MQTT_H_ */
