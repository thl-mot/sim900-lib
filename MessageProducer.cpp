/*
 * MessageProducer.cpp
 *
 *  Created on: 18.02.2018
 *      Author: thomas
 */

#include "MessageProducer.h"

MessageProducer::MessageProducer() {

}

MessageProducer::~MessageProducer() {

}

void MessageProducer::send(Stream *stream) {
	stream->available();
}

unsigned int MessageProducer::getSize() {
	return 0;
}

TextMessage::TextMessage(char *msg) {
	this->msg = msg;
}
TextMessage::~TextMessage() {

}
void TextMessage::send(Stream *stream) {
	stream->print( msg);
}
unsigned int TextMessage::getSize() {
	return strlen( msg);
}
