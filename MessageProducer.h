/*
 * MessageProducer.h
 *
 *  Created on: 18.02.2018
 *      Author: thomas
 */

#ifndef MESSAGEPRODUCER_H_
#define MESSAGEPRODUCER_H_

#include <Stream.h>

class MessageProducer {
public:
	MessageProducer();
	virtual ~MessageProducer();
	virtual void send( Stream *stream);
	virtual unsigned int getSize();
};

class TextMessage: public MessageProducer {
private:
	char *msg;
public:
	TextMessage( char *msg);
	virtual ~TextMessage();
	virtual void send( Stream *stream);
	virtual unsigned int getSize();
};

#endif /* MESSAGEPRODUCER_H_ */
